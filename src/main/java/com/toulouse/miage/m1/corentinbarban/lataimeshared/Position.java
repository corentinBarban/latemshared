package com.toulouse.miage.m1.corentinbarban.lataimeshared;

/*
 * Copyright (C) 2018 Rémi Venant $lt;remi.venant@gmail.com$gt;.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */


import java.io.Serializable;

public class Position implements Serializable{

    private static final long serialVersionUID = 1L;
    

    private double solde;

    /**
     * Constructeur de la classe Position
     * @param solde le solde du compte
     */
    public Position(double solde) {
        this.solde = solde;
    }

    
    /**
     * Récupérer le solde de la position
     * @return Le solde de la posititon
     */
    public double getSolde() {
        return solde;
    }

    /**
     * Modifier le solde de la position
     * @param solde Le nouveau solde
     */
    public void setSolde(double solde) {
        this.solde = solde;
    }
}
