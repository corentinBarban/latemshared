/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.services;

import com.toulouse.miage.m1.corentinbarban.lataimeshared.Position;
import exceptions.CompteInexistantException;
import exceptions.MontantInvalidException;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface ServiceBanqueRemote {
    
    /**
     * Permet de consulter la position du compte
     * @param idCompte Identifiant du compte bancaire
     * @return La position du compte bancaire
     * @throws CompteInexistantException L'identification du compte n'existe pas
     */
    public Position consulterPosition(Integer idCompte) throws CompteInexistantException;
    
    /*
     * Permet de créditer dans le compte idCompte
     * @param idCompte Identification du compte
     * @param montant Le montant à créditer
     * @throws CompteInexistantException L'identification du compte n'existe pas
     * @throws MontantInvalidException Le montant est inférieur à 0
     */
    public void crediter(Integer idCompte, Double montant) throws CompteInexistantException, MontantInvalidException;
    
    /*
     * Permet de récupérer la liste de tous les comptes existants
     * @return La liste de tous les comptes existants
     */
    public List<Integer> getComptes();
}
