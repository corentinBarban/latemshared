/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author mutatep
 */
public class MotDePasseIncorrectException extends Exception {

    /**
     * Creates a new instance of <code>MotDePasseIncorrectException</code>
     * without detail message.
     */
    public MotDePasseIncorrectException() {
    }

    /**
     * Constructs an instance of <code>MotDePasseIncorrectException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public MotDePasseIncorrectException(String msg) {
        super(msg);
    }
}
