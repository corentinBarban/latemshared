/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Corentin
 */
public class SoldeInsuffisantException extends Exception {

    /**
     * Creates a new instance of <code>soldeInsufissantException</code> without
     * detail message.
     */
    public SoldeInsuffisantException() {
    }

    /**
     * Constructs an instance of <code>soldeInsufissantException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public SoldeInsuffisantException(String msg) {
        super(msg);
    }
}
