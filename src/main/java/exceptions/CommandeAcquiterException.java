/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Corentin
 */
public class CommandeAcquiterException extends Exception {

    /**
     * Creates a new instance of <code>ClientInexistantException</code> without
     * detail message.
     */
    public CommandeAcquiterException() {
    }

    /**
     * Constructs an instance of <code>ClientInexistantException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public CommandeAcquiterException(String msg) {
        super(msg);
    }
}
