/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Corentin
 */
public class ProduitInexistanteException extends Exception {

    /**
     * Creates a new instance of <code>ProduitInexistanteException</code>
     * without detail message.
     */
    public ProduitInexistanteException() {
    }

    /**
     * Constructs an instance of <code>ProduitInexistanteException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public ProduitInexistanteException(String msg) {
        super(msg);
    }
}
