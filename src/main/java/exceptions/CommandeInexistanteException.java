/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Corentin
 */
public class CommandeInexistanteException extends Exception {

    /**
     * Creates a new instance of <code>commandeInexistanteException</code>
     * without detail message.
     */
    public CommandeInexistanteException() {
    }

    /**
     * Constructs an instance of <code>commandeInexistanteException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public CommandeInexistanteException(String msg) {
        super(msg);
    }
}
