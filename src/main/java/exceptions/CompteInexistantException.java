/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Corentin
 */
public class CompteInexistantException extends Exception {

    /**
     * Creates a new instance of <code>CompteInnexistantException</code> without
     * detail message.
     */
    public CompteInexistantException() {
    }

    /**
     * Constructs an instance of <code>CompteInnexistantException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public CompteInexistantException(String msg) {
        super(msg);
    }
}
