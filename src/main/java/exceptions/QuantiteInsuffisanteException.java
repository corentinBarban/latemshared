/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Corentin
 */
public class QuantiteInsuffisanteException extends Exception {

    /**
     * Creates a new instance of <code>QuantiteInsuffisanteException</code>
     * without detail message.
     */
    public QuantiteInsuffisanteException() {
    }

    /**
     * Constructs an instance of <code>QuantiteInsuffisanteException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public QuantiteInsuffisanteException(String msg) {
        super(msg);
    }
}
